package activitystreamer.server;

import java.util.HashMap;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import activitystreamer.server.models.*;
import activitystreamer.util.exception.RegisterFailException;
import activitystreamer.util.messages.*;

public class RegisterManager extends Thread {

	private static final Logger log = LogManager.getLogger();
	private HashMap<Connection, RegistrationLock> pendingLocks;

	public RegisterManager() {
		pendingLocks = new HashMap<Connection, RegistrationLock>();
		start();
	}

	/**
	 * remove the pending locks that is timed out.
	 */
	@Override
	public void run() {
		try {
			Thread.sleep(15000);
			refreshPendingLocks();
		} catch (InterruptedException e) {
			log.error(e.getMessage());
			e.printStackTrace();
		}
	}

	public boolean isIdUsed(String id) {
		for (RegistrationLock cn : pendingLocks.values()) {
			if (cn.client.getId().equals(id)) {
				return true;
			}
		}
		return false;
	}

	private synchronized void refreshPendingLocks() {
		for (Connection conn : pendingLocks.keySet()) {
			RegistrationLock lock = pendingLocks.get(conn);
			if (lock.hasTimedOut()) {
				pendingLocks.remove(conn);
			}
		}
	}

	/**
	 * manage local registration request. if local check ok, broadcast lock
	 * request
	 * 
	 * @return true, if the connection should be closed
	 * @throws RegisterFailException
	 */
	public synchronized String addRegistrationLock(RegistrationLock lock) {

		if (pendingLocks.containsKey(lock.conn)) {
			// Ignore registration attempt if submitted from a connection that
			// already sent one
			return null;
		} else if (isIdUsed(lock.client.getId())) {
			// Send REGISTER_FAIL if user name is in the process of being
			// registered at this server
			return lock.client.getId() + " is registering";
			// ControlSolution.getInstance().connectionClosed(lock.conn);

		} else if (ControlSolution.getInstance().allclients
				.containsKey(lock.client.getId())) {
			// Send REGISTER_FAIL if user name is already registered on this
			// server
			return lock.client.getId()
					+ " is already registered with the system";
		} else {
			// Otherwise, add to list of pending registration
			// and send LOCK_REQUEST to all connected servers
			pendingLocks.put(lock.conn, lock);

			if (lock.serverList.size() == 0) {
				releaseLock(lock);
			} else {
				LockRequestMsg lockReqMsg = new LockRequestMsg(
						lock.client.getId(), lock.client.getSecret());
				ControlSolution.getInstance().initServerBroadcast(
						lockReqMsg.toJsonString());
			}
			return null;
		}
	}

	public synchronized boolean processAllowMsg(LockAllowedMsg allowMsg) {
		for (Connection conn : pendingLocks.keySet()) {
			RegistrationLock lock = pendingLocks.get(conn);
			if (lock.client.auth(allowMsg.username, allowMsg.secret)) {
				lock.serverList.remove(allowMsg.server);
				if (lock.serverList.size() == 0) {
					releaseLock(lock);
					return true;
				}
			}
		}
		return false;
		// allowMsg.username
	}

	private synchronized void releaseLock(RegistrationLock lock) {
		RegisterSuccessMsg regSucMsg = new RegisterSuccessMsg(
				lock.client.getId());
		lock.conn.writeMsg(regSucMsg.toJsonString());
		ControlSolution.getInstance().allclients.put(lock.client.getId(),
				lock.client);
		ControlSolution.getInstance().connectedClients.put(lock.conn,
				lock.client);
		ControlSolution.getInstance().currentServer.setLoad(ControlSolution.getInstance().connectedClients.size());
		pendingLocks.remove(lock.conn);
	}

	public synchronized boolean processDenyMsg(LockDeniedMsg denyMsg) {
		for (Connection conn : pendingLocks.keySet()) {
			RegistrationLock lock = pendingLocks.get(conn);
			if (lock.client.auth(denyMsg.username, denyMsg.secret)) {
				denyLock(lock);
				return true;
			}
		}
		return false;
	}

	private synchronized void denyLock(RegistrationLock lock) {
		RegisterFailMsg regFailMsg = new RegisterFailMsg(lock.client.getId()
				+ " is already registered with the system");
		lock.conn.writeMsg(regFailMsg.toJsonString());
		lock.conn.closeCon();
		ControlSolution.getInstance().connectionClosed(lock.conn);
		pendingLocks.remove(lock.conn);
	}

}
