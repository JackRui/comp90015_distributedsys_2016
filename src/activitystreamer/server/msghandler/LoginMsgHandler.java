package activitystreamer.server.msghandler;

import java.util.Map.Entry;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import activitystreamer.server.Connection;
import activitystreamer.server.ControlSolution;
import activitystreamer.server.models.ClientNode;
import activitystreamer.server.models.ServerNode;
import activitystreamer.util.exception.InvalidMsgFormatException;
import activitystreamer.util.exception.LoginFailException;
import activitystreamer.util.messages.LoginMsg;
import activitystreamer.util.messages.LoginSuccessMsg;
import activitystreamer.util.messages.RedirectMsg;

// TODO: Need to implement redirect
public class LoginMsgHandler extends MessageHandler {

	@Override
	public boolean handleMsg(Connection sender, String msg)
			throws InvalidMsgFormatException, LoginFailException {

		LoginMsg loginObj;
		try {
			loginObj = new Gson().fromJson(msg, LoginMsg.class);
		} catch (JsonSyntaxException e) {
			throw new InvalidMsgFormatException(
					InvalidMsgFormatException.json_error_msg);
		}
		
		
		String name = loginObj.username;
		String secret = loginObj.secret;

		// Check message is valid
		if (loginObj.username.isEmpty()) {
			throw new InvalidMsgFormatException(
					"Login with no username attempted");
		}

		// If user name is anonymous, allow login without secret
		if (name.toLowerCase() == "anonymous") {
			ClientNode client = new ClientNode();
			LoginSuccessMsg replyObj = new LoginSuccessMsg(name);
			String replyMsg = replyObj.toJsonString();
			sender.writeMsg(replyMsg);
			// add the user to connected users list
			ControlSolution.getInstance().connectedClients.put(sender, client);
			ControlSolution.getInstance().currentServer.setLoad(ControlSolution.getInstance().connectedClients.size());
			considerRedirect(sender, client);

			// If user is non anonymous, check if user name is known
		} else if (ControlSolution.getInstance().allclients.containsKey(name)) {

			// If user name is known, check if secret is correct
			ClientNode client = ControlSolution.getInstance().allclients
					.get(name);
			Boolean isValidUser = client.auth(name, secret);

			// If user name and secret match, login is successful
			if (isValidUser) {
				LoginSuccessMsg replyObj = new LoginSuccessMsg(name);
				String replyMsg = replyObj.toJsonString();
				sender.writeMsg(replyMsg);
				// add the user to connected users list
				ControlSolution.getInstance().connectedClients.put(sender,
						client);
				ControlSolution.getInstance().currentServer.setLoad(ControlSolution.getInstance().connectedClients.size());
				// if redirect, return true to be automatically deleted by
				// Control
				return considerRedirect(sender, client);

				// If user name and secret do not match, login had failed
			} else {
				throw new LoginFailException();
			}

			// If user is not anonymous but not known, login had failed
		} else {
			throw new LoginFailException(LoginFailException.getNotRegMsg(name));
		}
		return false;
	}

	// Consider whether a new client that has been authenticated needs to be
	// redirected
	// and do the redirection if needed
	public boolean considerRedirect(Connection sender, ClientNode client) {

		for (Entry<String, ServerNode> server : ControlSolution.getInstance().allServers
				.entrySet()) {

			// If server is the current server, do nothing
			if (server.getKey().equals(
					ControlSolution.getInstance().currentServer.getId())) {
				continue;
			}

			// Check if the server has a load of 2 or less than the current
			// one
			// (at the load the current server would have if the client
			// connected)
			if (server.getValue().getLoad() <= ControlSolution.getInstance().currentServer
					.getLoad() - 2) {

				// Send redirect message
				RedirectMsg redirectObj = new RedirectMsg(
						server.getValue().host, server.getValue().port);
				sender.writeMsg(redirectObj.toJsonString());

				// Close the connection
				sender.closeCon();
				// ControlSolution.getInstance().connectedClients.remove(sender);
				// ControlSolution.getInstance().currentServer.decLoad();
				// return true instead if manually remove
				return true;

			}

		}
		return false;
	}
}
