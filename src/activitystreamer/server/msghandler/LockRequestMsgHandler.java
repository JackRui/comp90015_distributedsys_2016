package activitystreamer.server.msghandler;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import activitystreamer.server.Connection;
import activitystreamer.server.ControlSolution;
import activitystreamer.server.models.ClientNode;
import activitystreamer.util.exception.InvalidMsgFormatException;
import activitystreamer.util.messages.LockAllowedMsg;
import activitystreamer.util.messages.LockDeniedMsg;
import activitystreamer.util.messages.LockRequestMsg;

public class LockRequestMsgHandler extends MessageHandler {

	@Override
	public boolean handleMsg(Connection sender, String msg)
			throws InvalidMsgFormatException {
		// TODO Auto-generated method stub

		LockRequestMsg req;
		try {
			req = new Gson().fromJson(msg, LockRequestMsg.class);
		} catch (JsonSyntaxException e) {
			throw new InvalidMsgFormatException(
					InvalidMsgFormatException.json_error_msg);
		}
		
		if (req.secret.isEmpty() || req.username.isEmpty()) {
			throw new InvalidMsgFormatException(
					InvalidMsgFormatException.json_error_msg);
		}

		// Send LOCK_DENIED to all connected servers if user name is already
		// known by this server
		if (ControlSolution.getInstance().allclients.containsKey(req.username)) {
			LockDeniedMsg denyMsg = new LockDeniedMsg(req.username, req.secret);
			ControlSolution.getInstance().initServerBroadcast(
					denyMsg.toJsonString());
		}
		// if this server has a pending request that has the same username as a
		// remote request
		else if (ControlSolution.getInstance().regManager
				.isIdUsed(req.username)) {
			//deny it
			LockDeniedMsg denyMsg = new LockDeniedMsg(req.username, req.secret);
			ControlSolution.getInstance().initServerBroadcast(
					denyMsg.toJsonString());
			
		}
		// If user name is new, send LOCK_ALLOWED message to all connected
		// servers
		else {

			LockAllowedMsg allowMsg = new LockAllowedMsg(req.username,
					req.secret,
					ControlSolution.getInstance().currentServer.getId());
			ClientNode client = new ClientNode(req.username,
					req.secret);
			ControlSolution.getInstance().allclients.put(req.username, client);
			ControlSolution.getInstance().initServerBroadcast(
					allowMsg.toJsonString());
			ControlSolution.getInstance().relayServerBroadcast(sender, msg);
		}
		return false;

	}
}
