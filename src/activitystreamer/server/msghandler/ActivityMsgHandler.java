package activitystreamer.server.msghandler;

import org.json.simple.JSONObject;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import activitystreamer.server.Connection;
import activitystreamer.server.ControlSolution;
import activitystreamer.server.models.ClientNode;
import activitystreamer.util.exception.AuthenticateFailException;
import activitystreamer.util.exception.InvalidMsgFormatException;
import activitystreamer.util.messages.ActivityBroadcastMsg;
import activitystreamer.util.messages.ActivityMsg;

public class ActivityMsgHandler extends MessageHandler {

	
	@Override
	public boolean handleMsg(Connection sender, String msg)
			throws InvalidMsgFormatException, AuthenticateFailException {

		ActivityMsg actMsg;
		try {
			actMsg = new Gson().fromJson(msg, ActivityMsg.class);
		} catch (JsonSyntaxException e) {
			throw new InvalidMsgFormatException(
					InvalidMsgFormatException.json_error_msg);
		}

		if (actMsg.username.isEmpty() || actMsg.activity == null) {
			throw new InvalidMsgFormatException(
					InvalidMsgFormatException.json_error_msg);
		}

		if (!ControlSolution.getInstance().connectedClients.containsKey(sender)) {
			throw new AuthenticateFailException(
					AuthenticateFailException.clientFailMsg(actMsg.username));
		}

		ClientNode client = ControlSolution.getInstance().connectedClients
				.get(sender);
		if (!client.auth(actMsg.username, actMsg.secret)) {
			throw new AuthenticateFailException(
					AuthenticateFailException.clientFailMsg(actMsg.username));
		}
		// Process the activity object before broadcasting
		processActivityObj(actMsg.activity, actMsg.username);
		ActivityBroadcastMsg actBcMsg = new ActivityBroadcastMsg(
				actMsg.activity);

		// broadcast to both servers and clients
		ControlSolution.getInstance().initServerBroadcast(
				actBcMsg.toJsonString());
		ControlSolution.getInstance().initClientBroadcast(
				actBcMsg.toJsonString());

		return false;
	}

	@SuppressWarnings("unchecked")
	private void processActivityObj(JSONObject activity, String username) {
		activity.put("authenticated_user", username);
	}

}
