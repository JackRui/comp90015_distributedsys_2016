package activitystreamer.server.msghandler;

import activitystreamer.server.Connection;
import activitystreamer.server.ControlSolution;
import activitystreamer.server.models.ServerNode;
import activitystreamer.util.exception.AuthenticateFailException;
import activitystreamer.util.exception.InvalidMsgFormatException;
import activitystreamer.util.messages.AuthenticateMsg;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

public class AuthMsgHandler extends MessageHandler {

	@Override
	public boolean handleMsg(Connection sender, String msg)
			throws InvalidMsgFormatException, AuthenticateFailException {

		AuthenticateMsg authMsg;
		try {
			authMsg = new Gson().fromJson(msg, AuthenticateMsg.class);
		} catch (JsonSyntaxException e) {
			throw new InvalidMsgFormatException(
					InvalidMsgFormatException.json_error_msg);
		}

		if (authMsg.secret.isEmpty()) {
			throw new InvalidMsgFormatException(
					"Received AUTHENTICATION message did not have a secret field.");
		}
		// Get secret in message
		String msg_secret = authMsg.secret;
		// If secret incorrect, send "authentication fail message"
		if (!ControlSolution.getInstance().currentServer.auth(msg_secret)) {
			throw new AuthenticateFailException(
					AuthenticateFailException.serverFailMsg(msg_secret));
		}

		// If there is already a server in the connected server list,
		// this new server is already authenticated,
		// authenticate attempt is invalid
		if (ControlSolution.getInstance().connectedServers.containsKey(sender)
				&& ControlSolution.getInstance().connectedServers.get(sender)
						.isAuthenticated()) {
			throw new InvalidMsgFormatException(
					"Server is already authenticated");
		}

		// Create server node object
		String host = sender.getSocket().getRemoteSocketAddress().toString();
		int port = sender.getSocket().getPort();
		ServerNode newServer = new ServerNode("", msg_secret, host, port);

		// Add server to list of connected servers
		newServer.setAuthenticated(true);
		ControlSolution.getInstance().connectedServers.put(sender, newServer);
		return false;
	}
}
