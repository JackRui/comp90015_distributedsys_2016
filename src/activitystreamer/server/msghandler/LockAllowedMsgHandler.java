package activitystreamer.server.msghandler;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import activitystreamer.server.Connection;
import activitystreamer.server.ControlSolution;
import activitystreamer.util.exception.InvalidMsgFormatException;
import activitystreamer.util.messages.LockAllowedMsg;

public class LockAllowedMsgHandler extends MessageHandler {

	@Override
	public boolean handleMsg(Connection sender, String msg)
			throws InvalidMsgFormatException {
		// TODO Auto-generated method stub
		LockAllowedMsg allowMsg;
		try {
			allowMsg = new Gson().fromJson(msg, LockAllowedMsg.class);
		} catch (JsonSyntaxException e) {
			throw new InvalidMsgFormatException(
					InvalidMsgFormatException.json_error_msg);
		}
		if (allowMsg.server.isEmpty() || allowMsg.secret.isEmpty()
				|| allowMsg.username.isEmpty()) {
			throw new InvalidMsgFormatException(
					InvalidMsgFormatException.json_error_msg);
		}
		ControlSolution.getInstance().regManager.processAllowMsg(allowMsg);
		ControlSolution.getInstance().relayServerBroadcast(sender, msg);
		return false;
	}
}
