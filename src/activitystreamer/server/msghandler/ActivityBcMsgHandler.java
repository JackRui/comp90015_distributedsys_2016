package activitystreamer.server.msghandler;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import activitystreamer.server.Connection;
import activitystreamer.server.ControlSolution;
import activitystreamer.util.exception.InvalidMsgFormatException;
import activitystreamer.util.messages.ActivityBroadcastMsg;

public class ActivityBcMsgHandler extends MessageHandler {

	@Override
	public boolean handleMsg(Connection sender, String msg)
			throws InvalidMsgFormatException {

		ActivityBroadcastMsg bcMsg;
		try {
			bcMsg = new Gson().fromJson(msg, ActivityBroadcastMsg.class);
		} catch (JsonSyntaxException e) {
			throw new InvalidMsgFormatException(
					InvalidMsgFormatException.json_error_msg);
		}

		// if the user is authenticated
		if (ControlSolution.getInstance().connectedServers.containsKey(sender)
				&& ControlSolution.getInstance().connectedServers.get(sender)
						.isAuthenticated()) {
			ControlSolution.getInstance().relayServerBroadcast(sender,
					bcMsg.toJsonString());
			ControlSolution.getInstance().relayClientBroadcast(sender,
					bcMsg.toJsonString());
		} else {
			// send back an authentication failed msg
			throw new InvalidMsgFormatException("sender not authenticated");
		}
		return false;
	}
}
