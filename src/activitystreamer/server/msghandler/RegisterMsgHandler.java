package activitystreamer.server.msghandler;

import java.util.ArrayList;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import activitystreamer.server.Connection;
import activitystreamer.server.ControlSolution;
import activitystreamer.server.models.ClientNode;
import activitystreamer.server.models.RegistrationLock;
import activitystreamer.util.exception.InvalidMsgFormatException;
import activitystreamer.util.exception.RegisterFailException;
import activitystreamer.util.messages.RegisterMsg;

public class RegisterMsgHandler extends MessageHandler {

	@Override
	public boolean handleMsg(Connection sender, String msg)
			throws InvalidMsgFormatException, RegisterFailException {
		// Check for correct fields in the register message
		RegisterMsg regMsg;
		try {
			regMsg = new Gson().fromJson(msg, RegisterMsg.class);
		} catch (JsonSyntaxException e) {
			throw new InvalidMsgFormatException(
					InvalidMsgFormatException.json_error_msg);
		}

		if (regMsg.username.isEmpty() || regMsg.secret.isEmpty()) {
			throw new InvalidMsgFormatException(
					InvalidMsgFormatException.json_error_msg);
		}

		// Check if user is attempting to register 'anonymous' as a username
		if (regMsg.username.toLowerCase().equals("anonymous")) {
			throw new RegisterFailException(
					"Attempting to register anonymous as a username");
		}

		// If there is already a client on this connection, send INVALID_MESSAGE
		if (ControlSolution.getInstance().connectedClients.containsKey(sender)) {
			throw new InvalidMsgFormatException(
					"A client is already logged in on this connection");
		}

		// Create new node for client
		ClientNode tempClient = new ClientNode(regMsg.username, regMsg.secret);

		// Get list of all servers
		ArrayList<String> serverIdList = new ArrayList<String>(
				ControlSolution.getInstance().allServers.keySet());

		// Create registration lock for this new client and
		// add to list of current registration attempts
		RegistrationLock lock = new RegistrationLock(sender, tempClient,
				serverIdList);
		String addLockMsg = ControlSolution.getInstance().regManager
				.addRegistrationLock(lock);
		if (addLockMsg != null) {
			throw new RegisterFailException(addLockMsg);
		}
		return false;
	}
}
