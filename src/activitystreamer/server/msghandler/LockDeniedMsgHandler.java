package activitystreamer.server.msghandler;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import activitystreamer.server.Connection;
import activitystreamer.server.ControlSolution;
import activitystreamer.server.models.ClientNode;
import activitystreamer.util.exception.InvalidMsgFormatException;
import activitystreamer.util.messages.LockDeniedMsg;

public class LockDeniedMsgHandler extends MessageHandler {

	@Override
	public boolean handleMsg(Connection sender, String msg)
			throws InvalidMsgFormatException {
		// TODO Auto-generated method stub
		LockDeniedMsg denyMsg;
		try {
			denyMsg = new Gson().fromJson(msg, LockDeniedMsg.class);
		} catch (JsonSyntaxException e) {
			throw new InvalidMsgFormatException(
					InvalidMsgFormatException.json_error_msg);
		}

		if (denyMsg.secret.isEmpty() || denyMsg.username.isEmpty()) {
			throw new InvalidMsgFormatException(
					InvalidMsgFormatException.json_error_msg);
		}

		if (ControlSolution.getInstance().allclients
				.containsKey(denyMsg.username)) {
			ClientNode client = ControlSolution.getInstance().allclients
					.get(denyMsg.username);
			if (client.auth(denyMsg.username, denyMsg.secret)) {
				ControlSolution.getInstance().allclients
						.remove(denyMsg.username);
			}
		}

		ControlSolution.getInstance().regManager.processDenyMsg(denyMsg);
		ControlSolution.getInstance().relayServerBroadcast(sender, msg);

		return false;
	}

}
