package activitystreamer.server.msghandler;

import activitystreamer.server.Connection;

public abstract class MessageHandler {

	public abstract boolean handleMsg(Connection sender,
			String msg) throws Exception;
	
}
