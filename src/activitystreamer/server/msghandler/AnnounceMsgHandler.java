package activitystreamer.server.msghandler;

import com.google.gson.Gson;

import activitystreamer.server.Connection;
import activitystreamer.server.ControlSolution;
import activitystreamer.server.models.ServerNode;
import activitystreamer.util.messages.ServerAnnounceMsg;
import activitystreamer.util.exception.InvalidMsgFormatException;

public class AnnounceMsgHandler extends MessageHandler {

	@Override
	public boolean handleMsg(Connection sender, String msg)
			throws InvalidMsgFormatException {

		// Check that the sender is authenticated
		if (!ControlSolution.getInstance().connectedServers.containsKey(sender)
				|| !ControlSolution.getInstance().connectedServers.get(sender)
						.isAuthenticated()) {
			throw new InvalidMsgFormatException(
					"Sender of SERVER_ANNOUNCE is unauthenticated");
		}

		// Convert JSON object back to AnnounceMessage object
		Gson gson = new Gson();
		ServerAnnounceMsg msgObject = gson.fromJson(msg,
				ServerAnnounceMsg.class);

		// Check if ID in message is already known
		// use "put" to update or insert the value
		ServerNode server = new ServerNode(msgObject.id,
				ControlSolution.getInstance().currentServer.getSecret(),
				msgObject.hostname, msgObject.port);
		server.setLoad(msgObject.load);
		ControlSolution.getInstance().allServers.put(msgObject.id, server);

		// Broadcast to other connected servers other than sender
		ControlSolution.getInstance().relayServerBroadcast(sender, msg);

		return false;
	}

}
