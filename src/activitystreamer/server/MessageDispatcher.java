package activitystreamer.server;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import activitystreamer.server.msghandler.*;
import activitystreamer.util.exception.*;
import activitystreamer.util.messages.*;

public class MessageDispatcher {

	private static final Logger log = LogManager.getLogger();

	public static boolean processMsg(Connection con, String msg) {
		try {
			// log.debug(msg);
			JSONObject object = (JSONObject) JSONValue.parse(msg);
			if (!object.containsKey("command")) {

				throw new InvalidMsgFormatException(
						InvalidMsgFormatException.cmd_missing_msg);

			}
			String cmdType = object.get("command").toString();
			log.debug("Command reveived:" + cmdType);

			switch (cmdType) {
			case AuthenticateMsg._command:
				log.debug("Processing authentication attempt from new server");
				return new AuthMsgHandler().handleMsg(con, msg);
				
			case InvalidMsg._command:
				log.debug("INVALID MESSAGE: " + object.get("info"));
				con.closeCon();
				return true;
				
			case AuthFailMsg._command:
				log.debug("Authentication attempt has failed");
				con.closeCon();
				return true;
				
			case LoginMsg._command:
				log.debug("Client login attempt received");
				return new LoginMsgHandler().handleMsg(con, msg);
				
			case LogoutMsg._command:
				log.debug("Processing logout request");
				con.closeCon();
				return true;
				
			case ActivityMsg._command:
				log.debug("Processing activity message");
				return new ActivityMsgHandler().handleMsg(con, msg);
				
			case ServerAnnounceMsg._command:
				return new AnnounceMsgHandler().handleMsg(con, msg);
				
			case ActivityBroadcastMsg._command:
				return new ActivityBcMsgHandler().handleMsg(con, msg);
				
			case RegisterMsg._command:
				return new RegisterMsgHandler().handleMsg(con, msg);
				
			case LockRequestMsg._command:
				return new LockRequestMsgHandler().handleMsg(con, msg);
				
			case LockDeniedMsg._command:
				return new LockDeniedMsgHandler().handleMsg(con, msg);
				
			case LockAllowedMsg._command:
				return new LockAllowedMsgHandler().handleMsg(con, msg);
				
			default:
				throw new InvalidMsgFormatException(
						InvalidMsgFormatException.cmd_not_exist_msg);
			}
		} catch (InvalidMsgFormatException e) {
			// ControlSolution.getInstance().connectionClosed(sender);
			log.error("Invalid Message received");
			//log.error(msg);
			String reply = new InvalidMsg(e.getMessage()).toJsonString();
			con.writeMsg(reply);
			con.closeCon();
			return true;
		} catch (RegisterFailException e) {
			RegisterFailMsg failMsg = new RegisterFailMsg(e.getMessage());
			con.writeMsg(failMsg.toJsonString());
			con.closeCon();
			return true;
		} catch (AuthenticateFailException e) {
			// TODO Auto-generated catch block
			String reply = new AuthFailMsg(e.getMessage()).toJsonString();
			con.writeMsg(reply);
			con.closeCon();
			return true;
		}
		catch (LoginFailException e) {
			// TODO Auto-generated catch block
			String reply = new LoginFailMsg(e.getMessage()).toJsonString();
			con.writeMsg(reply);
			con.closeCon();
			return true;
		}

	}
}
