package activitystreamer.server.models;

import java.util.ArrayList;
import java.util.Date;

import activitystreamer.server.Connection;

public class RegistrationLock {
	public Connection conn;
	public ClientNode client;
	private Date requestTime;
	public ArrayList<String> serverList;

	public RegistrationLock(Connection conn, ClientNode client,
			ArrayList<String> serverList) {
		this.conn = conn;
		this.client = client;
		this.serverList = serverList;
		requestTime = new Date();
	}

	public boolean hasTimedOut(int miliseconds) {
		if (requestTime != null) {
			Date now = new Date();
			long timeElapsed = now.getTime() - requestTime.getTime();
			return timeElapsed > miliseconds;
		}
		return false;
	}

	public boolean hasTimedOut() {
		return hasTimedOut(15000);
	}
}
