package activitystreamer.server.models;

public class Node {
	protected String id = "";
	protected String secret = "";

	public String getId() {
		return id;
	}

	public String getSecret() {
		return secret;
	}

	@Override
	public boolean equals(Object o) {
		return this.id.equals(((Node) o).id)
				&& this.id.equals(((Node) o).secret);
	}
}
