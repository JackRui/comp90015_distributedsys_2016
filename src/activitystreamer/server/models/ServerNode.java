package activitystreamer.server.models;
 
import java.util.Date;

public class ServerNode extends Node {

	public String host = "";
	public int port = 0;
	private int currentLoad = 0;
	private boolean isAuthenticated = false;
	private Date lastAnnounceTime;

	public ServerNode(String id, String secret, String host, int port) {
		this.id = id;
		this.secret = secret;
		this.host = host;
		this.port = port;
	}

	public int getLoad() {
		return currentLoad;
	}

	public synchronized int setLoad(int load) {
		currentLoad = load;
		return currentLoad;
	}

	public void setAuthenticated(boolean authOk) {
		isAuthenticated = authOk;
	}

	public boolean isAuthenticated() {
		return isAuthenticated;
	}

	public boolean auth(String secret) {
		boolean ret = this.secret.equals(secret) && !secret.isEmpty();
		return ret;
	}

	public void updateStatus(int load, String host, int port) {
		lastAnnounceTime = new Date();
		this.currentLoad = load;
		this.host = host;
		this.port = port;
	}

	/**
	 * if the information has not been updated for 15 seconds.
	 * 
	 * @return true if timed out
	 */
	public boolean hasTimedOut() {
		if (lastAnnounceTime != null) {
			Date now = new Date();
			long timeElapsed = now.getTime() - lastAnnounceTime.getTime();
			return timeElapsed > 15000;
		}
		return false;
	}
}
