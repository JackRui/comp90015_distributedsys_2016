package activitystreamer.server.models;

public class ClientNode extends Node {

	// private boolean isOnline = false; // not used

	// default create anonymous client
	public ClientNode() {
		this.id = "anonymous";
		this.secret = "";
	}

	public ClientNode(String userName, String secret) {
		this.id = userName;
		this.secret = secret;
	}

	/*
	public void setOnline() {
		isOnline = true;
	}

	public void setOffline() {
		isOnline = false;
	}

	public boolean isOnline() {
		return isOnline;
	}
	*/

	public boolean auth(String id, String secret) {
		boolean ret = false;
		if (id.toLowerCase().equals("anonymous")) {
			return true;
		} else {
			ret = this.id.equals(id) && this.secret.equals(secret)
					&& !secret.isEmpty();
		}
		return ret;
	}

}
