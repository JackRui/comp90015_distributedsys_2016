package activitystreamer.server;

import java.io.IOException;
import java.net.Socket;
import java.util.HashMap;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import com.google.gson.Gson;

import activitystreamer.server.models.*;
import activitystreamer.util.messages.*;
import activitystreamer.util.Settings;

public class ControlSolution extends Control {
	private static final Logger log = LogManager.getLogger();

	/*
	 * additional variables as needed
	 */
	/*
	 * Use hash map to store servers and clients
	 */
	public RegisterManager regManager;
	public HashMap<Connection, ServerNode> connectedServers;
	public HashMap<Connection, ClientNode> connectedClients;

	// not includes this server itself
	public HashMap<String, ServerNode> allServers;
	// default includes anonymous user
	public HashMap<String, ClientNode> allclients;

	public ServerNode currentServer;
	private int countDown = 0;

	// since control and its subclasses are singleton, we get the singleton this
	// way
	public static ControlSolution getInstance() {
		if (control == null) {
			control = new ControlSolution();
		}
		return (ControlSolution) control;
	}

	public ControlSolution() {
		super();
		/*
		 * Do some further initialization here if necessary
		 */
		// initialize datasets used to store servers and clients data
		connectedServers = new HashMap<Connection, ServerNode>();
		connectedClients = new HashMap<Connection, ClientNode>();
		allServers = new HashMap<String, ServerNode>();
		allclients = new HashMap<String, ClientNode>();

		// If server is the first to start up in a system
		// (no remote host to connect to), set up a secret
		String secret;
		if (Settings.getRemoteHostname() == null) {
			secret = Settings.nextSecret();
		} else {
			secret = Settings.getSecret();
		}

		// Set up the server node's details
		String id = Settings.nextSecret();
		currentServer = new ServerNode(id, secret, Settings.getLocalHostname(),
				Settings.getLocalPort());

		// Display server ID and secret
		log.info("Server start [ID]: " + id + " [Secret]: " + secret);

		// Set up anonymous user name
		ClientNode anonymouseClient = new ClientNode();
		allclients.put(anonymouseClient.getId(), anonymouseClient);

		regManager = new RegisterManager();

		// check if we should initiate a connection and do so if necessary
		initiateConnection();
		// start the server's activity loop
		// it will call doActivity every few seconds
		start();
	}

	/*
	 * a new incoming connection
	 */
	@Override
	public Connection incomingConnection(Socket s) throws IOException {
		Connection con = super.incomingConnection(s);
		/*
		 * do additional things here
		 */

		return con;
	}

	/*
	 * a new outgoing connection
	 */
	@Override
	public Connection outgoingConnection(Socket s) throws IOException {
		Connection con = super.outgoingConnection(s);
		/*
		 * do additional things here
		 */

		// Create an "authenticate" message, Send it to connected server
		String authMsg = new Gson().toJson(new AuthenticateMsg(Settings
				.getSecret()));
		log.debug("Sending AUTHENTICATE message: " + authMsg);
		con.writeMsg(authMsg);

		// Create node for server that the current server is going to connect to
		// and add to list of connected servers
		ServerNode serverToConnect = new ServerNode("", Settings.getSecret(),
				Settings.getRemoteHostname(), Settings.getRemotePort());
		serverToConnect.setAuthenticated(true);
		connectedServers.put(con, serverToConnect);

		return con;
	}

	/*
	 * the connection has been closed
	 */
	@Override
	public void connectionClosed(Connection con) {
		super.connectionClosed(con);
		/*
		 * do additional things here
		 */
		if (connectedServers.containsKey(con)) {
			connectedServers.remove(con);
		}
		if (connectedClients.containsKey(con)) {
			connectedClients.remove(con);
			currentServer.setLoad(connectedClients.size());
		}
	}

	/*
	 * process incoming msg, from connection con return true if the connection
	 * should be closed, false otherwise
	 */
	@Override
	public synchronized boolean process(Connection con, String msg) {
		/*
		 * do additional work here return true/false as appropriate
		 */
		return MessageDispatcher.processMsg(con, msg);
	}

	/*
	 * Called once every few seconds Return true if server should shut down,
	 * false otherwise
	 */
	@Override
	public boolean doActivity() {
		/*
		 * do additional work here return true/false as appropriate
		 */
		// log.debug("Number of connected servers: " + connectedServers.size());
		// log.debug("Number of all servers: " + allServers.size());
		ServerAnnounceMsg announceMsg = new ServerAnnounceMsg(currentServer);
		String msg = new Gson().toJson(announceMsg);
		log.debug("Sending SERVER_ANNOUNCE: " + msg);
		initServerBroadcast(msg);
		refreshServerList();

		return false;
	}

	/*
	 * Other methods as needed
	 */

	/**
	 * start a broadcast, send to all connected servers
	 */
	public synchronized void initServerBroadcast(String msg) {
		for (Entry<Connection, ServerNode> server : connectedServers.entrySet()) {
			server.getKey().writeMsg(msg);
		}
	}

	/**
	 * send to all connected servers except the sender
	 */
	public synchronized void relayServerBroadcast(Connection sender, String msg) {
		for (Entry<Connection, ServerNode> server : connectedServers.entrySet()) {
			if (!server.getKey().equals(sender)) {
				server.getKey().writeMsg(msg);
			}
		}
	}

	/**
	 * send to all connected clients
	 */
	public synchronized void initClientBroadcast(String msg) {
		for (Entry<Connection, ClientNode> client : connectedClients.entrySet()) {
			client.getKey().writeMsg(msg);
		}
	}

	/**
	 * send to all connected clients except the sender
	 */
	public synchronized void relayClientBroadcast(Connection sender, String msg) {
		for (Entry<Connection, ClientNode> client : connectedClients.entrySet()) {
			if (!client.getKey().equals(sender)) {
				client.getKey().writeMsg(msg);
			}
		}
	}

	public String createAuthMsg(String secret) {
		// Create the JSON object
		AuthenticateMsg authMsg = new AuthenticateMsg(secret);
		String msgJsonStr = new Gson().toJson(authMsg);
		return msgJsonStr;
	}

	/**
	 * if the server in the list has timed out, remove it
	 */
	public synchronized void refreshServerList() {
		countDown--;
		// log.debug(countDown);
		if (countDown < 0) {
			log.debug("refresh server list");
			countDown = 3;
			for (String serverId : allServers.keySet()) {
				ServerNode server = allServers.get(serverId);
				if (server.hasTimedOut()) {
					allServers.remove(serverId);
				}
			}
		}
	}
}
