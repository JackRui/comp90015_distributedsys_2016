package activitystreamer.util.messages;

public class AuthFailMsg extends Message {
	public static final String _command = "AUTHENTICATION_FAIL";
	public final String command = _command;
	public String info = "";

	public AuthFailMsg(String info) {
		this.info = info;
	}
}
