package activitystreamer.util.messages;

public class RedirectMsg extends Message {
	public static final String _command = "REDIRECT";
	public final String command = _command;
	public String hostname = "";
	public int port = 0;
	
	public RedirectMsg(String hostname, int port) {
		this.hostname = hostname;
		this.port = port;
	}
}
