package activitystreamer.util.messages;

public class LockRequestMsg extends Message {
	public static final String _command = "LOCK_REQUEST";
	public final String command = _command;
	public String username = "";
	public String secret = "";

	public LockRequestMsg(String user, String secret) {
		this.username = user;
		this.secret = secret;
	}
}
