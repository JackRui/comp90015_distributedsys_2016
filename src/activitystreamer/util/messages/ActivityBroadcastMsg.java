package activitystreamer.util.messages;

import org.json.simple.JSONObject;

public class ActivityBroadcastMsg extends Message {
	public static final String _command = "ACTIVITY_BROADCAST";
	public final String command = _command;
	public JSONObject activity = null;

	public ActivityBroadcastMsg(JSONObject activity) {
		this.activity = activity;
	}
}
