package activitystreamer.util.messages;

public class AuthenticateMsg extends Message{
	public static final String _command = "AUTHENTICATE";
	public final String command = _command;
	public String secret = "";
	public AuthenticateMsg(String secret)
	{
		if (secret != null) {
			this.secret = secret;
		} 
	}
}
