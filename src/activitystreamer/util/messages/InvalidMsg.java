package activitystreamer.util.messages;

public class InvalidMsg extends Message {
	public static final String _command = "INVALID_MESSAGE";
	public String command = _command;
	public String info = "";
	
	public InvalidMsg(String info) {
		this.info = info;
	}
}
