package activitystreamer.util.messages;

import org.json.simple.JSONObject;

public class ActivityMsg extends Message {
	public static final String _command = "ACTIVITY_MESSAGE";
	public final String command = _command;
	public String username = "";
	public String secret = "";
	public JSONObject activity = null;

	public ActivityMsg(String username, String secret, JSONObject activity) {
		this.username = username;
		this.secret = secret;
		this.activity = activity;
	}
}
