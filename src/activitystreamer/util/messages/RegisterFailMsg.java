package activitystreamer.util.messages;

public class RegisterFailMsg extends Message {
	public static final String _command = "REGISTER_FAILED";
	public final String command = _command;
	public String info = "";

	public RegisterFailMsg(String info) {
		this.info = info;
	}
}
