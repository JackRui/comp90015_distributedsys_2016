package activitystreamer.util.messages;

public class LoginMsg extends Message {
	public static final String _command = "LOGIN";
	public final String command = _command;
	public String username = "";
	public String secret = "";

	public LoginMsg(String username, String secret) {
		this.username = username;
		this.secret = secret;
	}
}
