package activitystreamer.util.messages;

public class LoginSuccessMsg extends Message {
	public static final String _command = "LOGIN_SUCCESS";
	public final String command = _command;
	public String info = "";

	public LoginSuccessMsg(String username) {
		this.info = "logged in as user " + username;
	}
}
