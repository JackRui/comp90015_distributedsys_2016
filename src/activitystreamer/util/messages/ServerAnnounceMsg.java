package activitystreamer.util.messages;

import activitystreamer.server.models.ServerNode;

public class ServerAnnounceMsg extends Message {
	public static final String _command = "SERVER_ANNOUNCE";
	public final String command = _command;
	public String id = "";
	public int load = 0;
	public String hostname = "";
	public int port = 0;

	public ServerAnnounceMsg(ServerNode server) {
		this.id = server.getId();
		this.load = server.getLoad();
		this.hostname = server.host;
		this.port = server.port;
	}
}
