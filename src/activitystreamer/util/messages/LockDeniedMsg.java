package activitystreamer.util.messages;

public class LockDeniedMsg extends Message {
	public static final String _command = "LOCK_DENIED";
	public final String command = _command;
	public String username = "";
	public String secret = "";

	public LockDeniedMsg(String user, String secret) {
		this.username = user;
		this.secret = secret;
	}
}
