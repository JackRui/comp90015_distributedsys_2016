package activitystreamer.util.messages;

public class RegisterSuccessMsg extends Message {
	public static final String _command = "REGISTER_SUCCESS";
	public final String command = _command;
	public String info = "";

	public RegisterSuccessMsg(String user) {
		this.info = "register success for " + user;
	}
}
