package activitystreamer.util.messages;

public class RegisterMsg extends Message {
	public static final String _command = "REGISTER";
	public final String command = _command;
	public String username = "";
	public String secret = "";

	public RegisterMsg(String user, String secret) {
		this.username = user;
		this.secret = secret;
	}
}
