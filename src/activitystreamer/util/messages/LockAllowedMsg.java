package activitystreamer.util.messages;

public class LockAllowedMsg extends Message {
	public static final String _command = "LOCK_ALLOWED";
	public final String command = _command;
	public String username = "";
	public String secret = "";
	public String server = "";

	public LockAllowedMsg(String user, String secret, String serverid) {
		this.username = user;
		this.secret = secret;
		this.server = serverid;
	}
}
