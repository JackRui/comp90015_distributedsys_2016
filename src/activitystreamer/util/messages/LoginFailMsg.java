package activitystreamer.util.messages;

public class LoginFailMsg extends Message {
	public static final String _command = "LOGIN_FAILED";
	public final String command = _command;
	public String info = "attempt to login with wrong secret";

	public LoginFailMsg() {
	}

	public LoginFailMsg(String info) {
		this.info = info;
	}

}
