package activitystreamer.util.messages;

import com.google.gson.Gson;

public class Message {
	public String toJsonString() {
		Gson gson = new Gson();
		return gson.toJson(this);
	}

}
