package activitystreamer.util.messages;

public class LogoutMsg extends Message {
	public static final String _command = "LOGOUT";
	public final String command = _command;
}
