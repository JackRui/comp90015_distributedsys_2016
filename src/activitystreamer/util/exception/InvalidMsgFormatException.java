package activitystreamer.util.exception;

public class InvalidMsgFormatException extends Exception {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final String cmd_missing_msg = "the received message did not contain a command";
	public static final String cmd_not_exist_msg = "the command is not recognized.";
	public static final String json_error_msg = "JSON parse error while parsing message";

	public InvalidMsgFormatException(String msg) {
		super(msg);
	}

}
