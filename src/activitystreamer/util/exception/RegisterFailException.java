package activitystreamer.util.exception;

public class RegisterFailException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public RegisterFailException(String msg) {
		super(msg);
	}

}
