package activitystreamer.util.exception;

public class AuthenticateFailException extends Exception {
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public AuthenticateFailException(String msg) {
		super(msg);
	}
	public static String serverFailMsg(String secret) {
		return "the supplied secret is incorrect: " + secret;
	}

	public static String clientFailMsg(String username) {
		return "authentication failed for " + username;
	}
}
