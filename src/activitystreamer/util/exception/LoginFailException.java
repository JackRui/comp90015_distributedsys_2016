package activitystreamer.util.exception;

public class LoginFailException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public LoginFailException() {
		this("attempt to login with wrong secret");
	}

	public LoginFailException(String msg) {
		super(msg);
	}

	public static String getNotRegMsg(String user) {
		return "user " + user + " is not registered";
	}

}
