package activitystreamer.client;

import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.EOFException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;

import com.google.gson.Gson;

import activitystreamer.util.Settings;
import activitystreamer.util.exception.InvalidMsgFormatException;
import activitystreamer.util.messages.*;

public class ClientSolution extends Thread {
	private static final Logger log = LogManager.getLogger();
	private static ClientSolution clientSolution;
	private TextFrame textFrame;

	/*
	 * additional variables
	 */
	private Socket socket;
	private DataInputStream dataIn;
	private DataOutputStream dataOut;
	private BufferedReader inreader;
	private PrintWriter outwriter;

	public boolean keepRun = true;

	// this is a singleton object
	public static ClientSolution getInstance() {
		if (clientSolution == null) {
			clientSolution = new ClientSolution();
		}
		return clientSolution;
	}

	public ClientSolution() {
		/*
		 * some additional initialization
		 */
		// Initialize connection to a remote host
		initialize();

		// If user name is not 'anonymous' and no secret is given, register user
		// name
		// Otherwise, login
		if (Settings.getUsername().toLowerCase() != "anonymous"
				&& (Settings.getSecret() == null || Settings.getSecret().trim()
						.isEmpty())) {
			register(Settings.getUsername());
		} else {
			login();
		}
		// open the gui
		log.debug("opening the gui");
		textFrame = new TextFrame();
		// start the client's thread
		start();
	}

	// called by the gui when the user clicks "send"
	public void sendActivityObject(JSONObject activityObj) {
		log.debug("Begin sending activity object: " + activityObj.toString());
		ActivityMsg actMsg = new ActivityMsg(Settings.getUsername(),
				Settings.getSecret(), activityObj);
		Gson gson = new Gson();
		String msg = gson.toJson(actMsg);
		// String msg = activityObj.toString();
		log.debug("Sending activity message: " + msg);
		sendMsg(msg);
	}

	// called by the gui when the user clicks disconnect
	public void disconnect() {
		textFrame.setVisible(false);
		/*
		 * other things to do
		 */
		// Sends a logout message to server
		log.debug("Logout message sent");
		Gson gson = new Gson();
		LogoutMsg logoutObj = new LogoutMsg();
		String logoutStr = gson.toJson(logoutObj);
		sendMsg(logoutStr);
		closeConnection();
	}

	// Closes the connection
	public void closeConnection() {
		textFrame.setVisible(false);
		try {
			keepRun = false;
			if (socket != null && !socket.isClosed()) {
				inreader.close();
				outwriter.close();
				dataIn.close();
				dataOut.close();
				socket.close();
			}
		} catch (IOException e) {
			log.error(e.getMessage());
		}
		log.info("closing connection " + Settings.socketAddress(socket));
	}

	// the client's run method, to receive messages
	@Override
	public void run() {

		try {
			String response;
			while (keepRun && (response = inreader.readLine()) != null) {
				// output the received message
				log.debug(response);
				new MessageDispatcher().process(response);
			}
		} catch (SocketException e) {
			log.error(e.getMessage());
		} catch (EOFException e) {
			log.error(e.getMessage());
		} catch (IOException e) {
			log.error(e.getMessage());
		} catch (InvalidMsgFormatException e) {
			log.error(e.getMessage());
			// response to server
			String msg = new InvalidMsg(e.getMessage()).toJsonString();
			sendMsg(msg);
			closeConnection();
		}
	}

	/*
	 * additional methods
	 */
	public void displayMsg(JSONObject json) {
		textFrame.setOutputText(json);
	}

	public void initialize() {
		keepRun = true;
		try {
			socket = new Socket(Settings.getRemoteHostname(),
					Settings.getRemotePort());
			dataIn = new DataInputStream(socket.getInputStream());
			dataOut = new DataOutputStream(socket.getOutputStream());
			inreader = new BufferedReader(new InputStreamReader(dataIn));
			outwriter = new PrintWriter(dataOut, true);
		} catch (IOException e) {
			log.error(e.getMessage());
			closeConnection();
			e.printStackTrace();
		}
	}

	public void redirect(String host, int port) {
		closeConnection();
		Settings.setRemoteHostname(host);
		Settings.setRemotePort(port);

		// seems cannot restart the thread once stopped
		clientSolution = new ClientSolution();
	}

	public void sendMsg(String msg) {
		outwriter.println(msg);
		outwriter.flush();
	}

	public void register(String username) {
		String sec = Settings.nextSecret();
		Settings.setSecret(sec);
		log.info("New register. Secret generated: " + sec);
		RegisterMsg regMsg = new RegisterMsg(username, sec);
		String msgJson = new Gson().toJson(regMsg);
		sendMsg(msgJson);
	}

	public void login() {
		LoginMsg loginMsg = new LoginMsg(Settings.getUsername(),
				Settings.getSecret());
		String msgJson = new Gson().toJson(loginMsg);
		sendMsg(msgJson);
	}
}
