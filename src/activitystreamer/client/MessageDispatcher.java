package activitystreamer.client;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import activitystreamer.util.exception.InvalidMsgFormatException;
import activitystreamer.util.messages.*;

public class MessageDispatcher {
	private static final Logger log = LogManager.getLogger();

	public void process(String jsonStr) throws InvalidMsgFormatException {
		JSONObject json = null;
		try {
			JSONParser parser = new JSONParser();
			json = (JSONObject) parser.parse(jsonStr);
			if (!json.containsKey("command")) {
				throw new InvalidMsgFormatException(
						InvalidMsgFormatException.cmd_missing_msg);

			}

			String cmd = json.get("command").toString();
			switch (cmd) {
			case InvalidMsg._command:
			case AuthFailMsg._command: 
				//For when unauthenticated user send an activity message
			case LoginFailMsg._command:
			case RegisterFailMsg._command:
				// do the same thing with the 4 cmd type above
				log.debug("Message " + cmd + " received");
				handleFailureMsg(json);
				break;
			case LoginSuccessMsg._command:
				log.debug("Login Success");
				break;
			case RegisterSuccessMsg._command:
				log.debug("Register Success");
				break;
			case RedirectMsg._command:
				handleRedirect(json);
				break;
			case ActivityBroadcastMsg._command:
				handleActivityBc(json);
				break;
			default:
				log.debug(cmd);
				throw new InvalidMsgFormatException(
						InvalidMsgFormatException.cmd_not_exist_msg);
			}
		} catch (ParseException e) {
			e.printStackTrace();
			log.error(e.getMessage());
			throw new InvalidMsgFormatException(
					InvalidMsgFormatException.json_error_msg);
		}
	}

	private boolean handleFailureMsg(JSONObject json) {
		String info = json.get("info").toString();
		log.debug(info);
		ClientSolution.getInstance().closeConnection();
		return true;
	}

	private void handleRedirect(JSONObject json)
			throws InvalidMsgFormatException {
		Gson gson = new Gson();
		RedirectMsg msg;
		try {
			msg = gson.fromJson(json.toString(), RedirectMsg.class);
		} catch (JsonSyntaxException e) {
			throw new InvalidMsgFormatException(
					InvalidMsgFormatException.json_error_msg);
		}

		if (msg.hostname.isEmpty() | msg.port <= 0) {
			throw new InvalidMsgFormatException(
					InvalidMsgFormatException.json_error_msg);
		}

		log.debug("redirecting to " + msg.hostname + ":" + msg.port);

		ClientSolution.getInstance().redirect(msg.hostname, msg.port);
	}

	private void handleActivityBc(JSONObject json)
			throws InvalidMsgFormatException {
		// display activity message it receives
		Gson gson = new Gson();
		ActivityBroadcastMsg msg;
		try {
			msg = gson.fromJson(json.toString(), ActivityBroadcastMsg.class);
		} catch (JsonSyntaxException e) {
			throw new InvalidMsgFormatException(
					InvalidMsgFormatException.json_error_msg);
		}
		ClientSolution.getInstance().displayMsg(msg.activity);
	}

}
